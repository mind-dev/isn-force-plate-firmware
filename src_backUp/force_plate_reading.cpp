#ifndef FORCE_PLATE_READING_CPP
#define FORCE_PLATE_READING_CPP


#include "force_plate_reading.h"

#include <SPI.h>

#include <SingleEMAFilterLib.h>
SingleEMAFilter<double> singleEMAFilter(0.5);

#define cs 5   // chip select
#define rdy 14 // data ready, input
#define rst 25 // may omit

long Raw_Data = 0;

#define MAX_LEVEL_READING 15
double bufferReadingLevel[MAX_LEVEL_READING];
double maxLevel = 0;
int countLoop = 0;

#define LED 2 //to signal readiness
//default
// int filterADC = 3200;
#define TIME_REZERO 3000
// int filterADC = 0;//manual , rezero
int filterADCauto =0;
unsigned int rezeroDataSize = 0;
bool assignedADCfilter = false;
unsigned int initialSetTimeFP = 0;

#define SPISPEED 2500000 // Teensy 3.2 @120 x


void forcePlateSetup(){
  pinMode(cs, OUTPUT);
  digitalWrite(cs, LOW); // tied low is also OK.
  pinMode(rdy, INPUT);
  pinMode(rst, OUTPUT);
  digitalWrite(rst, LOW);
  delay(1);                // LOW at least 4 clock cycles of onboard clock. 100 microseconds is enough
  digitalWrite(rst, HIGH); // now reset to default values

  delay(500);
  SPI.begin(); //start the spi-bus
  delay(500);

  //init
  while (digitalRead(rdy))
  {
  }                                                                 // wait for ready_line to go low
  SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE1)); // start SPI
  digitalWrite(cs, LOW);
  delayMicroseconds(100);

  //Reset to Power-Up Values (FEh)
  SPI.transfer(0xFE);
  delay(5);

    byte status_reg = 0x00;  // address (datasheet p. 30)
                           // byte status_data = 0x01; // 01h = 0000 0 0 0 1 => status: Most Significant Bit First, Auto-Calibration Disabled, Analog Input Buffer Disabled
  byte status_data = 0x07; // 01h = 0000 0 1 1 1 => status: Most Significant Bit First, Auto-Calibration Enabled, Analog Input Buffer Enabled
  SPI.transfer(0x50 | status_reg);
  SPI.transfer(0x00);        // 2nd command byte, write one register only
  SPI.transfer(status_data); // write the databyte to the register
  delayMicroseconds(100);

   byte adcon_reg = 0x02; //A/D Control Register (Address 02h)
  //byte adcon_data = 0x20; // 0 01 00 000 => Clock Out Frequency = fCLKIN, Sensor Detect OFF, gain 1
  byte adcon_data = 0x00; // 0 00 00 000 => Clock Out = Off, Sensor Detect OFF, gain 1
  //byte adcon_data = 0x01;   // 0 00 00 001 => Clock Out = Off, Sensor Detect OFF, gain 2
  SPI.transfer(0x50 | adcon_reg); // 52h = 0101 0010
  SPI.transfer(0x00);             // 2nd command byte, write one register only
  SPI.transfer(adcon_data);       // write the databyte to the register
  delayMicroseconds(100);

  byte drate_reg = 0x03;  //DRATE: A/D Data Rate (Address 03h)
  byte drate_data = 0xF0; // F0h = 11110000 = 30,000SPS
  SPI.transfer(0x50 | drate_reg);
  SPI.transfer(0x00);       // 2nd command byte, write one register only
  SPI.transfer(drate_data); // write the databyte to the register
  delayMicroseconds(100);

  // Perform Offset and Gain Self-Calibration (F0h)
  SPI.transfer(0xF0);
  delay(400);
  digitalWrite(cs, HIGH);
  SPI.endTransaction();

  while (!Serial && (millis() <= initialSetTimeFP+5000))
    ; // WAIT UP TO 5000 MILLISECONDS FOR SERIAL OUTPUT CONSOLE
  Serial.println("configured, starting");
  Serial.println("");
  Serial.println("AIN0-AIN1    AIN2-AIN3    AIN4-AIN5    AIN6-AIN7");

  digitalWrite(LED,LOW);
  initialSetTimeFP = millis();

}

float forcePlateRead(){
  // Differential Measurements
  long adc_val[4] = {0, 0, 0, 0}; // store readings in array
  byte mux[4] = {0x01, 0x23, 0x45, 0x67};

  int i = 0;

  SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE1)); // start SPI
  digitalWrite(cs, LOW);
  delayMicroseconds(2);
for (i = 0; i <= 3; i++)
  {                        // read all 4 differential channel pairs AIN0-AIN1, AIN2-AIN3, AIN4-AIN5, AIN6-AIN7
    byte channel = mux[i]; // analog in channels #

    while (digitalRead(rdy))
    {
    };

    /*
 WREG: Write to Register
Description: Write to the registers starting with the register specified as part of the command. The number of registers that
will be written is one plus the value of the second byte in the command.
1st Command Byte: 0101 rrrr where rrrr is the address to the first register to be written.
2nd Command Byte: 0000 nnnn where nnnn is the number of bytes to be written – 1.
Data Byte(s): data to be written to the registers. 
 */
    //byte data = (channel << 4) | (1 << 3); //AIN-channel and AINCOM   // ********** Step 1 **********
    //byte data = (channel << 4) | (1 << 1)| (1); //AIN-channel and AINCOM   // ********** Step 1 **********
    //Serial.println(channel,HEX);
    SPI.transfer(0x50 | 0x01); // 1st Command Byte: 0101 0001  0001 = MUX register address 01h
    SPI.transfer(0x00);        // 2nd Command Byte: 0000 0000  1-1=0 write one byte only
    SPI.transfer(channel);     // Data Byte(s): xxxx 1000  write the databyte to the register(s)
    delayMicroseconds(2);

    //SYNC command 1111 1100                               // ********** Step 2 **********
    SPI.transfer(0xFC);
    delayMicroseconds(2);

    //while (!digitalRead(rdy)) {} ;
    //WAKEUP 0000 0000
    SPI.transfer(0x00);
    delayMicroseconds(250); // Allow settling time 250uS

    /*
MUX : Input Multiplexer Control Register (Address 01h)
Reset Value = 01h
BIT 7    BIT 6    BIT 5    BIT 4    BIT 3    BIT 2    BIT 1    BIT 0
PSEL3    PSEL2    PSEL1    PSEL0    NSEL3    NSEL2    NSEL1    NSEL0
Bits 7-4 PSEL3, PSEL2, PSEL1, PSEL0: Positive Input Channel (AINP) Select
0000 = AIN0 (default)
0001 = AIN1
0010 = AIN2 (ADS1256 only)
0011 = AIN3 (ADS1256 only)
0100 = AIN4 (ADS1256 only)
0101 = AIN5 (ADS1256 only)
0110 = AIN6 (ADS1256 only)
0111 = AIN7 (ADS1256 only)
1xxx = AINCOM (when PSEL3 = 1, PSEL2, PSEL1, PSEL0 are “don’t care”)
NOTE: When using an ADS1255 make sure to only select the available inputs.

Bits 3-0 NSEL3, NSEL2, NSEL1, NSEL0: Negative Input Channel (AINN)Select
0000 = AIN0
0001 = AIN1 (default)
0010 = AIN2 (ADS1256 only)
0011 = AIN3 (ADS1256 only)
0100 = AIN4 (ADS1256 only)
0101 = AIN5 (ADS1256 only)
0110 = AIN6 (ADS1256 only)
0111 = AIN7 (ADS1256 only)
1xxx = AINCOM (when NSEL3 = 1, NSEL2, NSEL1, NSEL0 are “don’t care”)
NOTE: When using an ADS1255 make sure to only select the available inputs.
 */

    SPI.transfer(0x01); // Read Data 0000  0001 (01h)       // ********** Step 3 **********
    delayMicroseconds(5);

    adc_val[i] = SPI.transfer(0);
    adc_val[i] <<= 8; //shift to left
    adc_val[i] |= SPI.transfer(0);
    adc_val[i] <<= 8;
    adc_val[i] |= SPI.transfer(0);
    delayMicroseconds(2);
  } // Repeat for each channel ********** Step 4 **********

  digitalWrite(cs, HIGH);
  SPI.endTransaction();

  //The ADS1255/6 output 24 bits of data in Binary Two's
  //Complement format. The LSB has a weight of
  //2VREF/(PGA(223 − 1)). A positive full-scale input produces
  //an output code of 7FFFFFh and the negative full-scale
  //input produces an output code of 800000h.

  for (i = 0; i <= 3; i++)
  { //4 cells
    if (adc_val[i] > 0x7fffff)
    {                                     //if MSB == 1
      adc_val[i] = adc_val[i] - 16777216; //do 2's complement
    }

    // if (long minus = adc_val[i] >> 23 == 1) //if the 24th bit (sign) is 1, the number is negative
    //    {
    //      adc_val[i] = adc_val[i] - 16777216;  //conversion for the negative sign
    //      //"mirroring" around zero
    //    }

    //    voltage = ((2*VREF) / 8388608)*adc_val[i]; //2.5 = Vref; 8388608 = 2^{23} - 1

    //Basically, dividing the positive range with the resolution and multiplying with the bits

    //    Serial.print(voltage, 8); //print it on serial, 8 decimals
    //    voltage = 0; //reset voltage
    //  Serial.print(float(micros())/100000,6)

    Raw_Data = +adc_val[i];
    // utk every cell
    //            Serial.print(adc_val[i]);   // Raw ADC integer value +/- 23 bits
    //            Serial.print("   ");
  }
  //            Raw_Data /= 4;

  // Raw_Data = Raw_Data - filterADC;

  // if (Raw_Data < 0)
  // {
  //   Raw_Data = 0;
  // }
  return (float)Raw_Data;

  // double presentData = Raw_Data;

  bufferReadingLevel[countLoop] = Raw_Data;
  countLoop++;

  if (countLoop >= MAX_LEVEL_READING)
  {

    maxLevel = bufferReadingLevel[0];

    for (int i = 1; i < countLoop; i++)
    {
      if (bufferReadingLevel[i] > maxLevel)
      {
        if (bufferReadingLevel[i] > 0)
        {
          maxLevel = bufferReadingLevel[i];
        }
      }
    }
    countLoop = 0;
  }
  singleEMAFilter.AddValue(maxLevel);
  // else if ((presentData < prevData) && ){
  //  prevData = presentData;
  //  memory = HIGH;
  //  Serial.print(30000);Serial.print("  ");
  //  Serial.print((float)micros()/1000000,6);Serial.print("  ");
  //  Serial.print(maxLevel);Serial.print("  ");
  
  //limit max min serial plotter. for debug only
  // Serial.print(0); Serial.print(" "); Serial.print(20000);Serial.print(" ");
  // passes signals with a frequency lower than a selected cutoff frequency
  // Serial.print(singleEMAFilter.GetLowPass()); 
  // Serial.print("  ");

  ///
// return singleEMAFilter.GetLowPass();

  //  double PostFilteredAnalogValue = AnalogValueLPF.Step(maxLevel);
  //  Serial.print(singleEMAFilter.GetHighPass());

  // Serial.println();

  //....Rest of program here

  //  delay(1); //Just to prevent spamming of Serial port
  // }
  // if (presentData == 0){
  //      Serial.print(12000);Serial.print("  ");
  //  Serial.print(PreFilteredAnalogValue);Serial.print("  ");
  ////  Serial.print(PostFilteredAnalogValue*2);//Serial.print("  ");
  //  Serial.println();
   
  // }


  //# after readilng
  //PostFilteredAnalogValue = AnalogValueLPF.Step(PreFilteredAnalogValue);
  // if  (millis() <= (TIME_REZERO + initialSetTimeFP) ){
  //   filterADCauto = (filterADCauto*rezeroDataSize + singleEMAFilter.GetLowPass())/(rezeroDataSize+1);
  //     rezeroDataSize++;
    
  // }else if (!assignedADCfilter){
  //   assignedADCfilter = true;
  //   filterADC = filterADCauto;
  //   digitalWrite(LED,HIGH);
  // }else{
  //   if (Firebase.ready() && (millis() - sendDataPrevMillis > 700 || sendDataPrevMillis == 0)){
  //     sendDataPrevMillis = millis();
  //         // Serial.printf("Set int... %s\n", Firebase.RTDB.setInt(&fbdo, FORCE_PLATE_ID , esp_random()) ? "ok" : fbdo.errorReason().c_str());
  //   //   Firebase.RTDB.setDouble(&fbdo, "/1/rawDataCurrent/" , singleEMAFilter.GetLowPass());    
  //         Serial.println("A reading");
  //   }
  // }



}


#endif