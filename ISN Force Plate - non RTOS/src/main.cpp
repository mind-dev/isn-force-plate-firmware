
#include "main.h"

void streamCallback(MultiPathStream stream)
{
  size_t numChild = sizeof(childPath) / sizeof(childPath[0]);

  for (size_t i = 0; i < numChild; i++)
  {
    if (stream.get(childPath[i]))
    {
      Serial.printf("path: %s, event: %s, type: %s, value: %s%s", stream.dataPath.c_str(), stream.eventType.c_str(), stream.type.c_str(), stream.value.c_str(), i < numChild - 1 ? "\n" : "");

      if (stream.dataPath == "/freeRun") //Not predefined measurement
      {
        if (stream.value == "true")
        {
          if (Firebase.RTDB.deleteNode(&fbdo, "/1/readings/"))
          {
            isStartFreeRun = true;
            Serial.println("Start Reading");
            startTimeGlobal = millis();
            packetNumGlobal = 0;
            // totalDataGlobal = 0;

            // Serial.println("Start Reading");
            // unsigned long endTime = millis() + round(stream.value.toFloat() * 1000);
            // unsigned long startTime = millis();
            // unsigned long totalData = 0;
            // String _time;
            // float dataPt;
            // unsigned int packetNum = 0;

            // Firebase.deleteNode(fbdo, "/1/readings/");

            // //SIGNAL USER 1
            // while (millis() < endTime)
            // {
            //   FirebaseJson jsonDataRead;
            //   for (unsigned int i = 0; (i < 100) && (millis() < endTime); i++)
            //   {
            //     dataPt = forcePlateRead();
            //     _time = String(millis() - startTime);
            //     totalData++;
            //     Serial.println(String(totalData) + ") " + _time + ". Data Pt: " + String(dataPt) + " >");
            //     jsonDataRead.set("/" + _time, dataPt);
            //   }
            //   packetNum++;
            //   Serial.println("===100===datas");
            //   Firebase.RTDB.setJSONAsync(&fbdo, "/1/readings/" + String(packetNum), &jsonDataRead);
            // }
            // Firebase.RTDB.setIntAsync(&fbdo, "/1/listener/readTime", 0);
          }
        }
        else /* if (stream.value == false) */
        {
          isStartFreeRun = false;
          // Serial.println("Stop");
          // packetNumGlobal =0;
        }
      }
      else if (stream.dataPath == "/getAvg") //for calibration and weighting
      {
        if (stream.value.toFloat() > 0)
        {
          long endTime = millis() + round(stream.value.toFloat() * 1000);
          double currentAvg = 0;
          // long count=0;
          for (long i = 0; millis() < endTime; i++)
          {
            currentAvg = (currentAvg * i + forcePlateRead()) / (i + 1);
            Serial.println(currentAvg);
          }
          Serial.print("The average for " + stream.value + "seconds :");
          Serial.println(currentAvg);
          Firebase.RTDB.setIntAsync(&fbdo, "/1/listener/getAvg", 0);
          Firebase.RTDB.setDoubleAsync(&fbdo, "/1/avgRawData", currentAvg);
        }
      }
      else if (stream.dataPath == "/readTime") //Measurement with predefined time
      {
        if (stream.value.toFloat() > 0)
        {
          // Serial.println("setting Array");
          // FirebaseJsonArray arraReadings;
          // FirebaseJsonArray arraTimes;
          // long endTime = millis() + round(stream.value.toFloat() * 1000);
          // long startTime = millis();
          // for (long i = 0; millis() < endTime; i++)
          // {
          //   arraReadings.add(forcePlateRead());
          //   // arraTimes.add(millis()-startTime);
          // }
          // Serial.println("done Reading");
          // // Firebase.RTDB.setArrayAsync(&fbdo, "/1/readings", &arraReadings);
          // // Firebase.RTDB.setArrayAsync(&fbdo, "/1/times", &arraTimes);
          // Firebase.RTDB.setIntAsync(&fbdo, "/1/listener/readTime", 0);

          //-- Send every 100 data pt
          if (Firebase.RTDB.deleteNode(&fbdo, "/1/readings/"))
          {

            Serial.println("Start Reading");
            unsigned long endTime = millis() + round(stream.value.toFloat() * 1000);
            unsigned long startTime = millis();
            unsigned long totalData = 0;
            String _time;
            float dataPt;
            unsigned int packetNum = 0;

            //SIGNAL USER 1
            while (millis() < endTime)
            {
              FirebaseJson jsonDataRead;
              for (unsigned int i = 0; (i < 100) && (millis() < endTime); i++)
              {

                dataPt = forcePlateRead();
                _time = String(millis() - startTime);
                totalData++;

                Serial.println(String(totalData) + ") " + _time + ". Data Pt: " + String(dataPt) + " >");

                jsonDataRead.set("/" + _time, dataPt); //for now just send times, twice

                // Firebase.RTDB.setDoubleAsync(&fbdo, readingsPath + _time, forcePlateRead());
                // Firebase.RTDB.setFloatAsync(&fbdo, readingsPath + _time, dataPt);

                // jsonDataRead.set("/" + String(totalData), String(millis() - startTime));
              }

              Serial.println("===100===datas");
              Firebase.RTDB.setJSONAsync(&fbdo, "/1/readings/" + String(packetNum), &jsonDataRead);
              packetNum++;
            }
            Firebase.RTDB.setIntAsync(&fbdo, "/1/listener/readTime", 0);
          }
        }
      }
      else if (stream.dataPath == "/realTime")
      {
      }
      else if (stream.dataPath == "/slowRT")
      {
        if (stream.value == "true")
        {
          //readForcePlate
          while (1)
          {
            Serial.println(forcePlateRead());
          }
        }
      }

      // switch (stream.dataPath)
      // {
      // case "/freeRun":
      //   break;
      // case "/getAvg":
      //
      //   break;
      // case "/readTime":
      //   break;
      // case "/realTime":
      //   break;
      // case "/slowRT":
      //   // like weight machine.
      //   if (stream.value == true)
      //   {
      //     //readForcePlate
      //     while (1)
      //     {
      //       Serial.println(forcePlateRead());
      //     }
      //   }
      //   break;

      // default:
      //   Serial.println("other dBchanges");
      //   break;
      // }
    }
  }

  Serial.println();

  //This is the size of stream payload received (current and max value)
  //Max payload size is the payload size under the stream path since the stream connected
  //and read once and will not update until stream reconnection takes place.
  //This max value will be zero as no payload received in case of ESP8266 which
  //BearSSL reserved Rx buffer size is less than the actual stream payload.
  Serial.printf("Received stream payload size: %d (Max. %d)\n\n", stream.payloadLength(), stream.maxPayloadLength());
}

void streamTimeoutCallback(bool timeout)
{
  if (timeout)
    Serial.println("stream timed out, resuming...\n");

  if (!stream.httpConnected())
    Serial.printf("error code: %d, reason: %s\n\n", stream.httpCode(), stream.errorReason().c_str());
}

void setup()
{

  Serial.begin(115200); //Initialising if(DEBUG)Serial Monitor
  pinMode(0, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  Serial.println();
  Serial.println("Disconnecting current wifi connection");
  WiFi.disconnect();
  EEPROM.begin(512); //Initialasing EEPROM using only 512 bytes
  delay(10);
  pinMode(15, INPUT);
  Serial.println();
  Serial.println();
  Serial.println("Startup");

  //---------------------------------------- Read eeprom for ssid and pass
  Serial.println("Reading EEPROM ssid");

  for (int i = 0; i < 32; ++i)
  {
    esid += char(EEPROM.read(i));
  }
  Serial.println();
  Serial.print("SSID: ");
  Serial.println(esid);
  Serial.println("Reading EEPROM pass");

  for (int i = 32; i < 96; ++i)
  {
    epass += char(EEPROM.read(i));
  }
  Serial.print("PASS: ");
  Serial.println(epass);

  WiFi.begin(esid.c_str(), epass.c_str());

  forcePlateSetup();
}
void loop()
{

  if ((WiFi.status() == WL_CONNECTED))
  {
    if (!isInitFirebase)
    {
      config.api_key = API_KEY;
      // auth.user.email = USER_EMAIL;
      // auth.user.password = USER_PASSWORD;

      config.database_url = DATABASE_URL;
      config.token_status_callback = tokenStatusCallback;
      config.signer.tokens.legacy_token = "n8CcD0xfvI6AUZBy7rT80AGEiLdev99imx8h8FVG";

      Firebase.begin(&config, &auth);

      Firebase.reconnectWiFi(true);
      Firebase.setDoubleDigits(5);

      FirebaseJson json;
      json.set("/freeRun", false);
      json.set("/getAvg", 0);
      json.set("/readTime", 0);
      json.set("/realTime", false);
      json.set("/slowRT", false);

      Firebase.RTDB.setJSONAsync(&fbdo, parentPath, &json);

      Serial.println("Listening");

      if (!Firebase.RTDB.beginMultiPathStream(&stream, parentPath))
      {
        Serial.printf("sream begin error, %s\n\n", stream.errorReason().c_str());
        isInitFirebase = false;
      }
      else
      {
        isInitFirebase = true;
      }
      Firebase.RTDB.setMultiPathStreamCallback(&stream, streamCallback, streamTimeoutCallback);
      digitalWrite(LED_BUILTIN, HIGH);
    }

    // for (int i = 0; i < 10; i++)
    // {
    //   Serial.print("Connected to ");
    //   Serial.print(esid);
    //   Serial.println(" Successfully");
    //   delay(100);
    // }
    // Listen to firebase

    // free run
    if (isStartFreeRun)
    {

      String _time;
      float dataPt;

      FirebaseJson jsonDataRead;
      for (unsigned int i = 0; (i < 100) && isStartFreeRun; i++)
      {

        dataPt = forcePlateRead();
        _time = String(millis() - startTimeGlobal);
        // totalDataGlobal++;

        // Serial.println(String(totalDataGlobal) + ") Time:" + _time + ". Data : " + String(dataPt) + " >Packet number" + String(packetNumGlobal));

        jsonDataRead.set("/" + _time, dataPt);
      }

      Serial.println("===100===datas");
      Firebase.RTDB.setJSONAsync(&fbdo, "/1/readings/" + String(packetNumGlobal), &jsonDataRead);
      packetNumGlobal++;
    }
  }
  else
  {
    // stream.endStream();
    isInitFirebase = false;
    digitalWrite(LED_BUILTIN, LOW);
  }

  if (digitalRead(0) == LOW)
  {
    Serial.println("clearing eeprom");
    for (int i = 0; i < 96; ++i)
    {
      EEPROM.write(i, 0);
    }
    EEPROM.commit(); // EEPROM.end : will also release the RAM copy
  }

  if (testWifi() && (digitalRead(15) != 1))
  {
    // Serial.println(" connection status positive");
    return;
  }
  else //if cannot connect to wifi, will become AP
  {
    Serial.println("Connection Status Negative / D15 HIGH");
    Serial.println("Turning the HotSpot On");
    launchWeb();
    setupAP(); // Setup HotSpot
  }

  Serial.println();
  Serial.println("Waiting.");

  while ((WiFi.status() != WL_CONNECTED))
  {
    // Serial.print(".");
    // delay(100);
    processDNS();
    // server.handleClient();
  }
  // delay(1000);
}

//----------------------------------------------- Fuctions used for WiFi credentials saving and connecting to it which you do not need to change
bool testWifi(void)
{
  int c = 0;
  //Serial.println("Waiting for Wifi to connect");
  while (c < 20)
  {
    if (WiFi.status() == WL_CONNECTED)
    {
      return true;
    }
    delay(500);
    Serial.print("*");
    c++;
  }
  Serial.println("");
  Serial.println("Connect timed out, opening AP");
  return false;
}

void launchWeb()
{
  Serial.println("");
  if (WiFi.status() == WL_CONNECTED)
    Serial.println("WiFi connected");
  Serial.print("Local IP: ");
  Serial.println(WiFi.localIP());
  Serial.print("SoftAP IP: ");
  Serial.println(WiFi.softAPIP());
  createWebServer();
  // Start the server
  // server.begin();
  // asyncServer.begin();
  beginAsyncServer(WiFi.softAPIP());
  Serial.println("Server started");
}

void setupAP(void)
{
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0)
    Serial.println("no networks found");
  else
  {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i)
    {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      //Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
      delay(10);
    }
  }
  Serial.println("");
  st = "<ol>";
  for (int i = 0; i < n; ++i)
  {
    // Print SSID and RSSI for each network found
    st += "<li>";
    st += WiFi.SSID(i);
    st += " (";
    st += WiFi.RSSI(i);

    st += ")";
    //st += (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*";
    st += "</li>";
  }
  st += "</ol>";
  delay(100);
  WiFi.softAP("Force Plate WiFi Manager", "");
  IPAddress local_IP(192, 168, 4, 1);
  IPAddress gateway(192, 168, 4, 9);
  IPAddress subnet(255, 255, 255, 0);
  Serial.println(WiFi.softAPConfig(local_IP, gateway, subnet) ? "Ready" : "Failed!");

  Serial.println("Initializing_softap_for_wifi credentials_modification");
  // const byte DNS_PORT = 53;
  // dnsServer.start(DNS_PORT, server_name, ap_IP);

  launchWeb();
  Serial.println("over");
}

// void createWebServer()
// {
//   {
//     server.on("/", []()
//               {

//                 IPAddress apIP = WiFi.softAPIP();
//                 String ipStr = String(apIP[0]) + '.' + String(apIP[1]) + '.' + String(apIP[2]) + '.' + String(apIP[3]);
//                 int nAP =  WiFi.scanNetworks();
//                 Serial.println(nAP);
//                 String listSSID = "<ol>";
//                 for (int i = 0; i < nAP ; ++i)
//                 {
//                   // Print SSID and RSSI for each network found
//                   listSSID += "<li>";
//                   listSSID += WiFi.SSID(i);
//                   listSSID += " (";
//                   listSSID += WiFi.RSSI(i);

//                   listSSID += ")";
//                   //st += (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*";
//                   listSSID += "</li>";
//                 }
//                 listSSID += "</ol>";
//                 Serial.println(listSSID);

//                 content = "<!DOCTYPE HTML>\r\n<html>Welcome to Wifi Credentials Update page";
//                 content += "<form action=\"/scan\" method=\"POST\"><input type=\"submit\" value=\"scan\"></form>";
//                 content += ipStr;
//                 content += "<p>";
//                 content += st;
//                 content += "</p><form method='get' action='setting'><label>SSID: </label><input name='ssid' length=32><input type='password' name='pass' length=64><input type='submit'></form>";
//                 content += "</html>";
//                 server.send(200, "text/html", content);
//               });
//     server.on("/scan", []()
//               {
//                 //setupAP();
//                 IPAddress apIP = WiFi.softAPIP();
//                 String ipStr = String(apIP[0]) + '.' + String(apIP[1]) + '.' + String(apIP[2]) + '.' + String(apIP[3]);

//                 content = "<!DOCTYPE HTML>\r\n<html>go back";
//                 server.send(200, "text/html", content);
//               });

//     server.on("/setting", []()
//               {
//                 String qsid = server.arg("ssid");
//                 String qpass = server.arg("pass");
//                 if (qsid.length() > 0 && qpass.length() > 0)
//                 {
//                   Serial.println("clearing eeprom");
//                   for (int i = 0; i < 96; ++i)
//                   {
//                     EEPROM.write(i, 0);
//                   }
//                   Serial.println("");
//                   Serial.println(qpass);
//                   Serial.println("");

//                   Serial.println("writing eeprom ssid:");
//                   for (int i = 0; i < qsid.length(); ++i)
//                   {
//                     EEPROM.write(i, qsid[i]);
//                     Serial.print("Wrote: ");
//                     Serial.println(qsid[i]);
//                   }
//                   Serial.println("writing eeprom pass:");
//                   for (int i = 0; i < qpass.length(); ++i)
//                   {
//                     EEPROM.write(32 + i, qpass[i]);
//                     Serial.print("Wrote: ");
//                     Serial.println(qpass[i]);
//                   }
//                   EEPROM.commit();

//                   content = "{\"Success\":\"saved to eeprom... reset to boot into new wifi\"}";
//                   statusCode = 200;
//                   ESP.restart();
//                 }
//                 else
//                 {
//                   content = "{\"Error\":\"404 not found\"}";
//                   statusCode = 404;
//                   Serial.println("Sending 404");
//                 }
//                 server.sendHeader("Access-Control-Allow-Origin", "*");
//                 server.send(statusCode, "application/json", content);
//               });

//               server.onNotFound(handle_NotFound);
//   }
// }

// void handle_NotFound(){
//   server.send(404, "text/plain", "Not found");
// }