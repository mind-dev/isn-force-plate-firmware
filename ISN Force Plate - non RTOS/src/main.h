#ifndef MAIN_H
#define MAIN_H

#include <Arduino.h>
#include "force_plate_reading.h"
#include "wifiManagerWebServer.h"

// https://www.youtube.com/watch?v=jfr2vNlyKmg&ab_channel=ElectronicsInnovation
// https://electronicsinnovation.com/change-esp32-wifi-credentials-without-uploading-code-from-arduino-ide/

#include <WiFi.h>
#include <HTTPClient.h>
// #include <WebServer.h>

// #include "ESPAsyncWebServer.h"


#include <EEPROM.h>

#include <Firebase_ESP_Client.h>

//Provide the token generation process info.
#include <addons/TokenHelper.h>
//Provide the RTDB payload printing info and other helper functions.
#include <addons/RTDBHelper.h>

// Put into own file later for security
#define API_KEY "AIzaSyBrEEX5P6tBET2vRcxjn49G92EVzm9JOUk"
#define DATABASE_URL "https://isn-forceplate-default-rtdb.asia-southeast1.firebasedatabase.app" //<databaseName>.firebaseio.com or <databaseName>.<region>.firebasedatabase.app
#define USER_EMAIL "esp32_1@mind.com"
#define USER_PASSWORD "password"

#ifndef LED_BUILTIN
#define LED_BUILTIN 2
#endif

FirebaseData fbdo;
FirebaseData stream;

String parentPath = "/1/listener";
String childPath[5] = {"/freeRun", "/getAvg", "/readTime", "/realTime", "/slowRT"};

String readingsPath = "/1/readings/";

FirebaseAuth auth;
FirebaseConfig config;

//Variables
int i = 0;
int statusCode;
//const char* ssid = "Default SSID";
//const char* passphrase = "Default passord";
const char *ssid = "MinD@unifi";
const char *passphrase = "mind1234";

bool isInitFirebase = false;

String st; //String to store the list of WiFi networks returned by WiFi.scanNetworks().
String content; //String to store the response from the server.
String esid;
String epass = "";

//Function Decalration
bool testWifi(void);
void launchWeb(void);
void setupAP(void);
void createWebServer(void); 
// http://192.168.4.1/

//Establishing Local server at port 80
// WebServer server(80);
// Create AsyncWebServer object on port 80
// AsyncWebServer asyncServer(80);
// Create an Event Source on /events
AsyncEventSource events("/events");

IPAddress ap_IP(192, 168, 4, 1);

const char *server_name = "www.myisnforceplate.com";


//___________________forceplate measurements

bool isInitForcePlate = false;
bool isStartFreeRun = false;
// bool deleteReadings = false;

unsigned long startTimeGlobal;
unsigned long totalDataGlobal = 0;
unsigned int packetNumGlobal = 0;

void handle_NotFound();

#endif