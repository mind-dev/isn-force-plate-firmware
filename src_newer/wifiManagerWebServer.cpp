#ifndef WIFIMANAGERWEBSERVER_CPP
#define WIFIMANAGERWEBSERVER_CPP

#include "wifiManagerWebServer.h"

const char *PARAM_INPUT_1 = "output";
const char *PARAM_INPUT_2 = "state";
int nWifi;

AsyncWebServer asyncServer(80);
DNSServer dnsServer;

class CaptiveRequestHandler : public AsyncWebHandler
{
public:
    CaptiveRequestHandler() {}
    virtual ~CaptiveRequestHandler() {}

    bool canHandle(AsyncWebServerRequest *request)
    {
        //request->addInterestingHeader("ANY");
        return true;
    }

    void handleRequest(AsyncWebServerRequest *request)
    {
        request->send_P(200, "text/html", index_html);
    }
};

void processDNS()
{
    dnsServer.processNextRequest();
}
void beginAsyncServer(IPAddress ip)
{
    //port 53 for DNS server, * for all IPs, IP of AP
    dnsServer.start(53, "*", ip);
    asyncServer.addHandler(new CaptiveRequestHandler()).setFilter(ON_AP_FILTER);
    asyncServer.begin();
    nWifi = WiFi.scanNetworks();
}

String processor(const String &var)
{
    //Serial.println(var);
    if (var == "BUTTONPLACEHOLDER")
    {
        // Add the list here
        String buttons = "";
        buttons += "<h4>Output - GPIO 2</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"2\" " + outputState(2) + "><span class=\"slider\"></span></label>";
        // buttons += "<h4>Output - GPIO 4</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"4\" " + outputState(4) + "><span class=\"slider\"></span></label>";
        // buttons += "<h4>Output - GPIO 33</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"33\" " + outputState(33) + "><span class=\"slider\"></span></label>";
        buttons += "<br>";
        return buttons;
    }
    else if (var == "WIFILIST")
    {
        String ssidList = "";
        ssidList += "<option value=\"testOnly\"> testOnly</option>";
        for (int i = 0; i < nWifi; ++i)
        {
            ssidList += "<option value=\""+ WiFi.SSID(i) + "\">" + WiFi.SSID(i) + "</option>";
            // ssidList += "<option value=\""+ String(i) + "\">" + String(i) + "</option>";
        }
        return ssidList;
    }
    return String();
}

String outputState(int output)
{
    if (digitalRead(output))
    {
        return "checked";
    }
    else
    {
        return "";
    }
}

void createWebServer()
{
    asyncServer.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
                   { request->send_P(200, "text/html", index_html, processor); });

    // Send a GET request to <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
    asyncServer.on("/update", HTTP_GET, [](AsyncWebServerRequest *request)
                   {
                       String inputMessage1;
                       String inputMessage2;
                       // *PARAM_INPUT_1 = "output";
                       // *PARAM_INPUT_2 = "state";
                       // GET input1 value on <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
                       if (request->hasParam(PARAM_INPUT_1) && request->hasParam(PARAM_INPUT_2))
                       {
                           inputMessage1 = request->getParam(PARAM_INPUT_1)->value();
                           inputMessage2 = request->getParam(PARAM_INPUT_2)->value();
                           digitalWrite(inputMessage1.toInt(), inputMessage2.toInt());
                       }
                       else
                       {
                           inputMessage1 = "No message sent";
                           inputMessage2 = "No message sent";
                       }
                       Serial.print("GPIO: ");
                       Serial.print(inputMessage1);
                       Serial.print(" - Set to: ");
                       Serial.println(inputMessage2);
                       request->send(200, "text/plain", "OK");
                   });
}

#endif